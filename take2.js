return Promise.try(function () {
    return MongoClient.connectAsync(MONGODB_CONNECTION_STRING)
}).then(function(db){
    return Promise.try(function(){
        return db.collection("downloads").findOneAsync({key: status.filename});
    }).then(function(item){
        return Promise.try(function() {
            return db.closeAsync();
        }).then(function() {
            return item;
        });
    });
}).then(function(content) {
    // ... 
});